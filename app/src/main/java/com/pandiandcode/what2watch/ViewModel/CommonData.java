package com.pandiandcode.what2watch.ViewModel;

import com.pandiandcode.what2watch.Model.Film;

import java.util.List;

/**
 * Created by Perrankana on 13/05/15.
 */
public class CommonData {

    private List<Film> films;

    private String errorMessage ="";

    private static CommonData instance = new CommonData();

    private CommonData(){}

    public static CommonData getInstance(){
        return instance;
    }

    public void setFilms(List<Film> films){
        this.films = films;
    }

    public Film getFilm(int position){
        return films.get(position);
    }

    public List<Film> getFilms(){
        return films;
    }

    public void setErrorMessage(String error){
        errorMessage = error;
    }

    public String getErrorMessage(){
        return errorMessage;
    }
}
