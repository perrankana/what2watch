package com.pandiandcode.what2watch.ViewModel;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pandiandcode.what2watch.Model.Film;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Perrankana on 13/05/15.
 */
public class APIEndPoint {

    private static final String API_URL = "http://www.omdbapi.com/?";

    /**
     * Search and Get from the API a list of films
     * @param search
     * @return List of Films
     */
    public static List<Film> getFilms(String search){

        List<Film> films = new ArrayList<>();

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet();

        try {

            request.setURI(new URI(API_URL + "s=" + URLEncoder.encode(search, "UTF-8")));

            // Set Headers
            request.setHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);

            // Get Status Code
            int statusCode = response.getStatusLine().getStatusCode();

            // Response OK
            if (statusCode == 200) {

                BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                StringBuilder sb = new StringBuilder("");
                String line;
                String NL = System.getProperty("line.separator");

                while ((line = in.readLine()) != null)
                    sb.append(line).append(NL);

                in.close();
                String page = sb.toString();
                Log.d("API", page);
                JSONObject jresponse = new JSONObject(page);

                // if JSON object has a Search key, everything is ok
                if (jresponse.has("Search")) {

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();
                    Type listType = new TypeToken<ArrayList<Film>>() {}.getType();
                    films = gson.fromJson(jresponse.getString("Search"), listType);

                } else if (jresponse.has("Error")) {

                    CommonData.getInstance().setErrorMessage(jresponse.getString("Error"));

                } else {

                    CommonData.getInstance().setErrorMessage("UNKNOW");

                }

            } else {

                CommonData.getInstance().setErrorMessage("UNKNOW");

            }

            return films;

        } catch (URISyntaxException | IOException | JSONException e) {
            CommonData.getInstance().setErrorMessage(e.getLocalizedMessage());
            return films;
        }
    }

    public static Film getFilm(String imdbID){

        Film film;
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet();

        try {

            request.setURI(new URI( API_URL + "i="+imdbID+"&plot=Short"));

            // Set Headers
            request.setHeader("Accept", "application/json");
            HttpResponse response = client.execute(request);

            // Get Status Code
            int statusCode = response.getStatusLine().getStatusCode();

            // Response OK
            if(statusCode == 200) {

                BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                StringBuilder sb = new StringBuilder("");
                String line;
                String NL = System.getProperty("line.separator");

                while ((line = in.readLine()) != null)
                    sb.append(line).append(NL);

                in.close();
                String page = sb.toString();

                Gson gson = new Gson();
                film = gson.fromJson(page, Film.class);

                return film;

            } else {

                CommonData.getInstance().setErrorMessage("UNKNOW");
                return null;
            }


        } catch (URISyntaxException | IOException e) {
            CommonData.getInstance().setErrorMessage(e.getLocalizedMessage());
            return null;
        }

    }

}