package com.pandiandcode.what2watch.AsyncTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.pandiandcode.what2watch.Activities.MainActivity;
import com.pandiandcode.what2watch.Model.Film;
import com.pandiandcode.what2watch.R;
import com.pandiandcode.what2watch.ViewModel.APIEndPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Perrankana on 13/05/15.
 */
public class APITask extends AsyncTask<String,Integer,List<Film>> {

    private static String PROGRESSDIALOG = "PROGRESSDIALOG";
    private OnCallCompleted mListener;
    private ProgressDialog mProgressDialog;
    private Context context;

    public APITask(Context context){
        this.mListener = (OnCallCompleted)context;
        this.context = context;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        // Create and setup ProgressDialog
        mProgressDialog = new ProgressDialog(context);
        // show progressBar and save it in RetainerFragment
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setTitle(R.string.searching);
        mProgressDialog.show();
        ((MainActivity)context).getRetainerFragmentManager().put(PROGRESSDIALOG, mProgressDialog);
    }

    @Override
    protected List doInBackground(String[] params) {

        List<Film> films = new ArrayList<>();

        // Get film list from the APIEndPoint with the search parameter provided as params[0]
        List<Film> tempFilms = APIEndPoint.getFilms(params[0]);

        // Loop through the list and get from the APIEndPoint the detailed info of the film
        for(Film film : tempFilms){
            films.add(APIEndPoint.getFilm(film.getImdbID()));
        }
        return films;

    }

    @Override
    protected void onPostExecute(List<Film> list) {
        super.onPostExecute(list);

        // Tell the Listener that the download of info has finished
        mListener.onCallCompleted(list);

        // Dismiss the ProgressBar
        if(mProgressDialog!=null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    public interface OnCallCompleted{
        void onCallCompleted(List<Film> films);
    }
}
