package com.pandiandcode.what2watch.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.HashMap;

/**
 * Created by Perrankana on 18/04/15.
 *
 * This class is used to restore the state after hardware configurations changes
 */
public class RetainerFragmentManager {

    private final String mRetainedFragmentTag;

    private final FragmentManager mFragmentManager;

    private RetainerFragment mRetainerFragment;

    public RetainerFragmentManager(Activity activity, String retainedFragmentTag){

        mFragmentManager = ((FragmentActivity)activity).getSupportFragmentManager();
        mRetainedFragmentTag = retainedFragmentTag;

    }

    /**
     *
     * @return true if it is the RetainerFragment hasn't been added to the FragmentManager
     * false otherwise
     */
    public boolean firstTimeIn(){

        mRetainerFragment = (RetainerFragment)mFragmentManager.findFragmentByTag(mRetainedFragmentTag);

        if(mRetainerFragment != null ){

            return false;

        }else{

            // Add RetainerFragment to the Stack
            mRetainerFragment = new RetainerFragment();
            mFragmentManager.beginTransaction().add(mRetainerFragment,mRetainedFragmentTag).commit();

            return true;
        }


    }

    /**
     * Find Fragment in the Stack
     * @param tag
     * @return Fragment
     */
    public Fragment findFragment(String tag){
        return mFragmentManager.findFragmentByTag(tag);
    }

    /**
     * Add Fragment to the Stack
     * @param containerID
     * @param fragment
     * @param tag
     */
    public void addFragment(int containerID, Fragment fragment, String tag){

        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.add(containerID, fragment, tag);
        ft.commit();

    }

    /**
     * Replace Fragment and add it to the backStack
     * @param containerID
     * @param fragment
     * @param tag
     */
    public void replaceFragment(int containerID, Fragment fragment, String tag){

        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.replace(containerID,fragment,tag);
        ft.addToBackStack(tag);
        ft.commit();

    }

    /**
     * @param tag
     * @return true is the Fragment is resumed and the user can interact, false otherwise
     */
    public boolean isFragmentResumed(String tag){

        Fragment fragment = mFragmentManager.findFragmentByTag(tag);

        if ( fragment !=null ) {

            return fragment.isResumed();

        }

        return false;

    }

    /**
     * Put Object in RetainerFragment
     * @param key
     * @param object
     */
    public void put(String key, Object object){
        mRetainerFragment.put(key, object);
    }

    /**
     * Get Object from the RetainerFragment
     * @param key
     * @param <T>
     * @return Object
     */
    public <T> T get(String key){
        return (T) mRetainerFragment.get(key);
    }

    /**
     * Remove Object from the RetainerFragment
     * @param key
     */
    public void remove(String key){
        mRetainerFragment.remove(key);
    }

    public static class RetainerFragment extends Fragment{

        private HashMap<String, Object> mData = new HashMap<>();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setRetainInstance(true);
        }

        public void put(String key,Object object){
            mData.put(key, object);
        }

        public <T> T get(String key){
            return (T) mData.get(key);
        }

        public void remove(String key){
            mData.remove(key);
        }
    }
}
