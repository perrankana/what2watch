package com.pandiandcode.what2watch.Fragments;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pandiandcode.what2watch.Model.Film;
import com.pandiandcode.what2watch.R;
import com.pandiandcode.what2watch.ViewModel.CommonData;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Perrankana on 13/05/15.
 */
public class FilmFragment extends Fragment {

    private RelativeLayout mFilmLayout;
    private ImageView mPoster;
    private TextView mTitle;
    private TextView mYear;
    private TextView mGenre;
    private TextView mDirector;
    private TextView mActors;
    private TextView mPlot;
    private TextView mCountry;
    private TextView mAwards;
    private TextView mRating;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View mView = inflater.inflate(R.layout.film_fragment, container, false );
        mFilmLayout = (RelativeLayout)mView.findViewById(R.id.film_layout);
        mPoster = (ImageView)mView.findViewById(R.id.imgPoster);
        mTitle = (TextView)mView.findViewById(R.id.textTitle);
        mYear = (TextView)mView.findViewById(R.id.textYear);
        mGenre = (TextView)mView.findViewById(R.id.textGenre);
        mDirector = (TextView)mView.findViewById(R.id.textDirector);
        mActors = (TextView)mView.findViewById(R.id.textActors);
        mPlot = (TextView)mView.findViewById(R.id.textPlot);
        mCountry = (TextView)mView.findViewById(R.id.textCountry);
        mAwards = (TextView)mView.findViewById(R.id.textAwards);
        mRating = (TextView)mView.findViewById(R.id.textRating);

        Bundle args = getArguments();

        int position = 0;

        if ( args != null )
            position = args.getInt( "position" );

        Film film = CommonData.getInstance().getFilm(position);

        fillData(film);

        return mView;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void fillData(Film film){

        Picasso.with(getActivity())
                .load( film.getPoster() )
                .resize( (int)getActivity().getResources().getDimension(R.dimen.img_small_width), (int)getActivity().getResources().getDimension(R.dimen.img_small_height) )
                .centerCrop()
                .placeholder( R.drawable.holder )
                .error( R.drawable.holder )
                .into(mPoster, new Callback() {
                    @Override
                    public void onSuccess() {
                        onFinishImageLoad();
                    }

                    @Override
                    public void onError() {
                        onFinishImageLoad();
                    }
                });

        setupTextView(mTitle, film.getTitle());
        setupTextView(mYear, film.getYear());
        setupTextView(mGenre, film.getGenre());
        setupTextView(mDirector, film.getDirector());
        setupTextView(mActors, film.getActors());
        setupTextView(mPlot, film.getPlot());
        setupTextView(mCountry, film.getCountry());
        setupTextView(mAwards, film.getAwards());
        setupTextView(mRating, film.getImdbRating());

    }

    /**
     * Change Background Color of the layout
     * @param backgroundColor
     */
    private void changeColors(int backgroundColor){

        mFilmLayout.setBackgroundColor(backgroundColor);

    }

    /**
     * function fired when the image downloaded has finished.
     * Using Palette, it gets the main colors of the bitmap in an async way.
     */
    private void onFinishImageLoad(){


        Palette.PaletteAsyncListener listener = new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette palette) {
                Palette.Swatch swatch = palette.getMutedSwatch();

                // Change background color
                changeColors(swatch.getRgb());
            }
        };
        BitmapDrawable bitmapDrawable = (BitmapDrawable)mPoster.getDrawable();
        if(bitmapDrawable!=null)
            Palette.generateAsync(bitmapDrawable.getBitmap(),listener);

    }

    /**
     * Show the textView if the text is a valid one, otherwise hide the textView
     * @param textView
     * @param text
     */
    private void setupTextView(TextView textView, String text){

        if ( text.equals("N/A") || text.equals("n/a") ) {

            textView.setVisibility(View.GONE);

        } else {

            textView.setText(text);
            textView.setVisibility(View.VISIBLE);

        }

    }

    /**
     * Update all view with the new information
     * @param position
     */

    public void update(int position){

        Film film = CommonData.getInstance().getFilm(position);
        fillData(film);

    }

}
