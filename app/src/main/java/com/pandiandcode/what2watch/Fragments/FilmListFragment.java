package com.pandiandcode.what2watch.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.pandiandcode.what2watch.Adapters.FilmListAdapter;
import com.pandiandcode.what2watch.R;
import com.pandiandcode.what2watch.ViewModel.CommonData;

/**
 * Created by Perrankana on 13/05/15.
 */
public class FilmListFragment extends Fragment {

    private GridView mFilmGridView;
    private FilmListAdapter mListAdapter;

    private FilmGridListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View mView = inflater.inflate(R.layout.film_list_fragment, container, false );

        mFilmGridView = (GridView)mView.findViewById(R.id.gridFilmView);

        // Create Adapter with the film list
        mListAdapter = new FilmListAdapter(getActivity(), CommonData.getInstance().getFilms());

        // Set Adapter to the GridView
        mFilmGridView.setAdapter(mListAdapter);

        mFilmGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Tell the listener that an item has been clicked
                mListener.onGridItemSelected(position);
            }
        });

        return mView;

    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        mListener = (FilmGridListener)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * Update GridView
     */
    public void update(){

        mListAdapter.setFilms(CommonData.getInstance().getFilms());
        mListAdapter.notifyDataSetChanged();

    }

    public interface FilmGridListener{
        void onGridItemSelected(int position);
    }
}
