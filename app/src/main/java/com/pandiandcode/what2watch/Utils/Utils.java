package com.pandiandcode.what2watch.Utils;

import android.content.Context;
import android.widget.Toast;

import com.pandiandcode.what2watch.ViewModel.CommonData;

/**
 * Created by Perrankana on 14/05/15.
 */
public class Utils {

    public static void showToast(Context context, String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public static void showError(Context context){
        showToast(context, CommonData.getInstance().getErrorMessage());
    }

}
