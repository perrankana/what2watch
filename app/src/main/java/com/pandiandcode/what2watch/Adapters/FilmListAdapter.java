package com.pandiandcode.what2watch.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pandiandcode.what2watch.Model.Film;
import com.pandiandcode.what2watch.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Perrankana on 13/05/15.
 */
public class FilmListAdapter extends BaseAdapter {

    private Context context;
    private List<Film> films;

    public FilmListAdapter(Context context, List<Film> films){
        this.context = context;
        this.films = films;
    }

    @Override
    public int getCount() {
        return films.size();
    }

    @Override
    public Object getItem(int position) {
        return films.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        Film film = ( Film )getItem( position );
        View viewToUse;

        LayoutInflater mInflater = ( LayoutInflater ) context .getSystemService( Activity.LAYOUT_INFLATER_SERVICE );

        if ( convertView == null ) {

            viewToUse = mInflater.inflate( R.layout.film_list_item, null );

            holder = new ViewHolder();

            holder.mImageFilm = ( ImageView )viewToUse.findViewById( R.id.imgFilm );
            holder.mTitle = ( TextView )viewToUse.findViewById( R.id.textTitle );
            holder.mLayout = (RelativeLayout)viewToUse.findViewById(R.id.item_layout);

            viewToUse.setTag( holder );

        } else {

            viewToUse = convertView;
            holder = ( ViewHolder ) viewToUse.getTag();

        }

        if ( film != null ) {

            holder.mTitle.setText(film.getTitle());
            Picasso.with(context)
                    .load(film.getPoster())
                    .resize((int) context.getResources().getDimension(R.dimen.img_small_width), (int) context.getResources().getDimension(R.dimen.img_small_height))
                    .centerCrop()
                    .placeholder(R.drawable.holder)
                    .error(R.drawable.holder)
                    .into(holder.mImageFilm, new Callback() {
                        @Override
                        public void onSuccess() {
                            onFinishImageLoad(holder);
                        }

                        @Override
                        public void onError() {
                            onFinishImageLoad(holder);
                        }
                    });


        }

        return viewToUse;

    }

    private void onFinishImageLoad(final ViewHolder holder){
        Palette.PaletteAsyncListener listener = new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette palette) {
                Palette.Swatch swatch = palette.getMutedSwatch();
                holder.mLayout.setBackgroundColor(swatch.getRgb());
                holder.mTitle.setBackgroundColor(swatch.getRgb());
                holder.mTitle.setTextColor(swatch.getTitleTextColor());

            }
        };
        BitmapDrawable bitmapDrawable = (BitmapDrawable)holder.mImageFilm.getDrawable();
        if(bitmapDrawable!=null)
            Palette.generateAsync(bitmapDrawable.getBitmap(),listener);
    }

    public void setFilms(List<Film> films){
        this.films = films;
    }

    class ViewHolder{
        ImageView mImageFilm;
        TextView mTitle;
        RelativeLayout mLayout;
    }
}
