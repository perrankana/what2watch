package com.pandiandcode.what2watch.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;

import com.pandiandcode.what2watch.AsyncTask.APITask;
import com.pandiandcode.what2watch.Fragments.FilmFragment;
import com.pandiandcode.what2watch.Fragments.FilmListFragment;
import com.pandiandcode.what2watch.Fragments.RetainerFragmentManager;
import com.pandiandcode.what2watch.Model.Film;
import com.pandiandcode.what2watch.R;
import com.pandiandcode.what2watch.Utils.Utils;
import com.pandiandcode.what2watch.ViewModel.CommonData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Perrankana on 13/05/15.
 */
public class MainActivity extends FragmentActivity implements APITask.OnCallCompleted, FilmListFragment.FilmGridListener {

    private static String PROGRESSDIALOG = "PROGRESSDIALOG";
    private static String FILMS = "FILMS";

    private static String DEFAULT_SEARCH = "Swan";

    private static String GRID_FILM_FRAGMENT = "GridFilmFragment";
    private static String FILM_FRAGMENT = "FilmFragment";

    private boolean isFilmFragment = false;

    private EditText mSearch;

    private ProgressDialog mProgressDialog;

    protected RetainerFragmentManager mRetainerFragmentManager = new RetainerFragmentManager(this,"MainActivity");;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_layout);

        mSearch = (EditText)findViewById(R.id.editSearch);

        if( mRetainerFragmentManager.firstTimeIn() ){

            // This is the first time this Activity is created
            updateGridFilmFragment(new ArrayList<Film>());

            // Start AsyncTask to download films
            new APITask(this).execute(DEFAULT_SEARCH);

        } else {

            // This is not the first time the Activity is created
            // Get the list of films from the RetainerFragment, if the list is null, the AsyncTask is still running
            List<Film> films = mRetainerFragmentManager.get(FILMS);

            if( films == null ){

                // AsyncTask still running, show the ProgressBar
                mProgressDialog = mRetainerFragmentManager.get(PROGRESSDIALOG);
                if(mProgressDialog!=null) {
                    mProgressDialog.show();
                }

            } else {

                // AsyncTask has finished, the Fragments can be updated
                updateGridFilmFragment(films);

            }

        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Dismiss AsyncTask ProgressBar
        mProgressDialog = mRetainerFragmentManager.get(PROGRESSDIALOG);
        if(mProgressDialog!=null)
            mProgressDialog.dismiss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Update the content of the FilmListFragment
     * @param films
     */
    public void updateGridFilmFragment(List<Film> films){

        // store films in CommonData to be accessed from FilmListFragment
        CommonData.getInstance().setFilms(films);

        // check if the fragment has been added in the stack
        Fragment fragment = mRetainerFragmentManager.findFragment(GRID_FILM_FRAGMENT);

        if (fragment == null) {

            // Fragment is not added, create fragment and add it to the stack
            fragment = new FilmListFragment();
            mRetainerFragmentManager.addFragment(R.id.fragment_container, fragment, GRID_FILM_FRAGMENT);

        } else {

            // Fragment is added, update its content
            ((FilmListFragment) fragment).update();

        }

    }

    /**
     * Add and show FilmFragment to the Activity
     * @param position
     */
    public void showFilmFragment(int position){

        // check if the fragment has been added in the stack
        Fragment fragment = mRetainerFragmentManager.findFragment(FILM_FRAGMENT);

        if ( fragment == null ) {

            // Fragment is not added, create fragment and add it to the stack passing parameters through a Bundle
            Bundle args = new Bundle();
            args.putInt( "position", position );

            fragment = new FilmFragment();
            fragment.setArguments(args);

            mRetainerFragmentManager.replaceFragment(R.id.fragment_container, fragment, FILM_FRAGMENT);

        } else {

            // Fragment is added, update its content
            ((FilmFragment)fragment).update(position);

        }

    }

    /**
     * Functions fires when the search button receives onClick event
     * @param view
     */
    public void searchFilms(View view){

        // Get Search parameter from the EditText View
        String search = mSearch.getText().toString();

        if ( search.equals("") ) {

            // Search parameter is empty, show toast
            Utils.showToast(this, getResources().getString(R.string.search_empty));

        } else {

            // Check if a FilmFragment is resumed (Film single view)
            isFilmFragment = mRetainerFragmentManager.isFragmentResumed(FILM_FRAGMENT);

            // Remove films from the RetainerFragment
            mRetainerFragmentManager.remove(FILMS);
            // run AsyncTask to get Films
            new APITask(this).execute(mSearch.getText().toString());

        }
    }

    /**
     * hook method fired when the AsyncTask has finished
     * @param films
     */
    @Override
    public void onCallCompleted( List<Film> films ) {

        // if films are empty show why
        if(films.isEmpty())
            Utils.showError(this);

        else {

            // save films in RetainerFragment
            mRetainerFragmentManager.put(FILMS, films);

            // update FilmListFragment, and if it is in Film Single view update FilmFragment too with
            // the first film on the list.
            updateGridFilmFragment(films);
            if (isFilmFragment)
                showFilmFragment(0);
        }
    }

    /**
     * hook method fired when an Item in FilmListFragment is selected
     * @param position
     */
    @Override
    public void onGridItemSelected(int position) {
        showFilmFragment(position);
    }

    public RetainerFragmentManager getRetainerFragmentManager(){
        return  mRetainerFragmentManager;
    }
}
