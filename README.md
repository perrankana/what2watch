# README #

This App uses [OMDb](http://www.omdbapi.com/) API. This is a free web service to obtain movie information.

This App shows in a GridView the results of the movie search as an Image and Text. If users click on an Item of the GridView, the App will show more detailed info about that film.
